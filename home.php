<!DOCTYPE HTML>
<html>
    <head>
        <?php include('head.php');?>
        <title>TT 2016-B093: INICIO</title>
    </head>
    <body>
        <header>
            <div class="imagenlogo"><img src="../imagenes/logo_sitio.png" width="250" height="50" alt=""></div>
            <div class="logo">Programa Prioritario de Epilepsia</div>
        </header>
        <div class="contenedor-formulario">
            <div class="wrapH">
                <form class="formulario" action="POST" href="#">
                    <div class="input_group">
                        <input type="text" name="user" id="usuario">
                        <label class="label active" for="usuario">Usuario:</label>
                    </div>
                    <div class="input_group">
                        <input id="contrasena" type="password" name="pass">
                        <label class="label active" for="contrasena">Contrase&ntilde;a:</label>
                    </div>
                </form>
                <button class="btn btn-outline-primary" type="submit" name="Ingresar" >Ingresar</button>
                <a class="btn btn-outline-warning"  href="#">&iquest;Olvidaste contrase&ntilde;a?</a>
                <a class="btn btn-outline-primary" href="#" data-toggle="modal">Registrarse</a>
            </div>
        </div>
        <footer>
            <div class="texto">Av. Insurgentes Sur No. 3877, Col. La Fama, Del. Tlalpan, - Tel. 5606 3822
Distrito Federal CP. 14269
            </div>
        </footer>
    </body>
</html>