<?php

$error=0;

if(isset($_POST['RegistrarPaciente'])){
    $banderaprinc=curp($_POST['curp']);
    if(empty($_POST['curp'])) $error=1;
    else if($banderaprinc==1) $error=2;
    else if($banderaprinc==2) $error=3;
    else if($banderaprinc==3) $error=4;
    else if($banderaprinc==4) $error=5;
    else if($banderaprinc==5) $error=6;
    else if($banderaprinc==6) $error=7;
    else if($banderaprinc==7) $error=8;
    else if($banderaprinc==8) $error=9;
    else if($banderaprinc==9) $error=10;
    else if($banderaprinc==10) $error=11;
    else if((is_numeric($_POST['nombreP']))==TRUE) $error=17;
    else if((empty($_POST['nombreP']))||($_POST['nombreP']==" ")) $error=14;
    else if((is_numeric($_POST['apellidoPP']))==TRUE) $error=16;
    else if((empty($_POST['apellidoPP']))||($_POST['apellidoPP']==" ")) $error=12;
    else if((is_numeric($_POST['apellidoMP']))==TRUE) $error=15;
    else if((empty($_POST['apellidoMP']))||($_POST['apellidoMP']==" ")) $error=13;
    else if((empty($_POST['tipoCrisis']))||($_POST['tipoCrisis']==" ")) $error=18;
    else if((empty($_POST['celP1']))||($_POST['celP1']==" ")) $error=19;
    else{
        $error=0;
    }
    mostrarerror($error,$_POST['curp']);
}

/*----VALIDAR CURP------*/
function validarEdo($id){
    $edo=array("AS","BC","BS","CC","CS","CH","CL","CM","DF","DG","GT","GR","HG","JC","MC","MN","MS","MT","NL","OC","PL","QT","QR","SP","SL","SR","TC","TS","TL","VZ","YN","ZS","aS","bC","bS","cC","cS","cH","cL","cM","dF","dG","gT","gR","hG","jC","mC","mN","mS","mT","nL","oC","pL","qT","qR","sP","sL","sR","tC","tS","tL","vZ","yN","zS","As","Bc","Bs","Cc","Cs","Ch","Cl","Cm","Df","Dg","Gt","Gr","Hg","Jc","Mc","Mn","Ms","Mt","Nl","Oc","Pl","Qt","Qr","Sp","Sl","Sr","Tc","Ts","Tl","Vz","Yn","Zs","as","bc","bs","cc","cs","ch","cl","cm","df","dg","gt","gr","hg","jc","mc","mn","ms","mt","nl","oc","pl","qt","qr","sp","sl","sr","tc","ts","tl","vz","yn","zs");
    $bandera4=0;
    if(in_array($id,$edo)){
        $bandera4=1;
    }
    return $bandera4;
}

function validarFecha($idday,$idyear,$idmonth){
    if(($idday>29)&&($idmonth==2)) return $valor=1;
    else if((($idday==29)&&($idmonth==2))&&(($idyear%4)!=0)) return $valor=2;
    else if(($idmonth==4||$idmonth==6||$idmonth==9||$idmonth==11)&&($idday==31)) return $valor=3;
    else return $valor=0;
}

function mostrarerror($error,$id){
    if($error==1) echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Falta ingresar la CURP</div>';
    else if($error==2) echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>La CURP no est&aacute; completa</div>';
    else if($error==3) echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Hay al menos un n&uacute;mero en los 4 primeros digitos</div>';
    else if($error==4) echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>En los 4 primeros digitos hay un caracter que no es una letra</div>';
    else if($error==5) echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Febrero no tiene m&aacute;s de 28 d&iacute;as</div>';
    else if($error==6) echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>El a&ntilde;o no es bisiesto</div>';
    else if($error==7) echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Abril, Junio, Septiembre y Noviembre no tienen m&aacute;s de 30 d&iacute;as</div>';
    else if($error==8) echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>El 11vo campo de la CURP no es alguna letra siguente: H,h,M,m</div>';
    else if($error==9) echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Los dos campos del Estado no son v&aacute;lidos</div>';
    else if($error==10) echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>No todos los tres siguientes campos del sexo son letras</div>';
    else if($error==11) echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Los &uacute;ltimos dos campos de la CURP no son n&uacute;meros</div>';
    else if($error==12) echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Falta el Apellido Paterno</div>';
    else if($error==13) echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Falta el Apellido Materno</div>';
    else if($error==14) echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Falta el Nombre</div>';
    else if($error==15) echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>El apellido paterno no debe tener n&uacute;meros</div>';
    else if($error==16) echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>El apellido materno no debe tener n&uacute;meros</div>';
    else if($error==17) echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>El nombre no debe tener n&uacute;meros</div>';
    else if($error==18) echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Falta ingresar el Tipo de Crisis</div>';
    else if($error==19) echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Falta ingresar el n&uacute;mero de celular</div>';
    else{
        for($i=4;$i<6;$i++) $idyear[$i-4]=$id[$i];
        for($i=6;$i<8;$i++) $idmonth[$i-6]=$id[$i];
        for($i=8;$i<10;$i++) $idday[$i-8]=$id[$i];
        $cadenaanio=implode($idyear);
        $cadenames=mes($idmonth);
        $cadenadia=implode($idday);   
        echo $cadenaanio.'-'.$cadenames.'-'.$cadenadia;
        $a=(int) $cadenaanio;
        $d=(int) $cadenadia;
        echo $a.'-'.$cadenames.'-'.$d;
        if(!($conectar=mysqli_connect("localhost","root"))){
            echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Error al realizar la conexi&oacute;n</div>';
            die('Error al con'.mysql_error());
        }
        else{
            mysqli_select_db($conectar,"TT2016B093");
            mysqli_query($conectar,"SET NAMES 'utf8'");
            $querry="INSERT INTO PACIENTE(Curp,Nombre,ApellidoP,ApellidoM,NacA,NacM,NacD,TipoCrisis,Direccion) VALUES('".$_POST['curp']."','".$_POST['nombreP']."','".$_POST['apellidoPP']."','".$_POST['apellidoMP']."',$a,'".$cadenames."',$d,'".$_POST['tipoCrisis']."','".$_POST['direcc']."')";
            $result=mysqli_query($conectar,$querry);
            if(!$result){
                echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Error al realizar registro</div>';
                die('Error al realizar el registro en la bd'.mysql_error());
            }
            else echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>Datos ingresados correctamente</div>';
            mysqli_close($conectar);
        }
    }
}

function mes($idmonth){
    if($idmonth==1) return $mes="Enero";
    else if($idmonth==2) return $mes="Febrero";
    else if($idmonth==3) return $mes="Marzo";
    else if($idmonth==4) return $mes="Abril";
    else if($idmonth==5) return $mes="Mayo";
    else if($idmonth==6) return $mes="Junio";
    else if($idmonth==7) return $mes="Julio";
    else if($idmonth==8) return $mes="Agosto";
    else if($idmonth==9) return $mes="Septiembre";
    else if($idmonth==10) return $mes="Octubre";
    else if($idmonth==11) return $mes="Noviembre";
    else return $mes="Diciembre";
}

function curp($id){
    $error2=0;
    $bandera1=0;
    $bandera=0;
    $bandera2=0;
    $bandera5=0;
    if(strlen($id)<18||strlen($id)>18){
        $error2=1;
        return $error2;
    }
    else{
        for($i=0;$i<4;$i++) $idparte1[$i]=$id[$i];
        for($i=0;$i<4;$i++){if(is_numeric($idparte1[$i])){$bandera1=1; break;}}
        if($bandera1==1){//LOS 4 PRIMEROS VALORES SON NUMEROS?
            $error2=2;
            return $error2;
        }
        else{
            //LOS 4 PRIMEROS VALORES SON LETRAS?
            $cadena=implode($idparte1);
            if(ctype_alpha($cadena)) {$bandera=1; echo $cadena;}
            if($bandera==0){
                $error2=3;
                return $error2;
            }
            else{
                for($i=4;$i<6;$i++) $idyear[$i-4]=$id[$i];
                for($i=6;$i<8;$i++) $idmonth[$i-6]=$id[$i];
                for($i=8;$i<10;$i++) $idday[$i-8]=$id[$i];
                $anio=implode($idyear);
                $mes=implode($idmonth);
                $dia=implode($idday);
                echo $anio;
                echo $mes;
                echo $dia;
                settype($anio,"int");
                settype($mes,"int");
                settype($dia,"int");
                $bandera2=validarFecha($dia,$anio,$mes);
                if($bandera2==1){
                    $error2=4;
                    return $error2;
                }
                else if($bandera2==2){
                    $error2=5;
                    return $error2;
                }
                else if($bandera2==3){
                    $error2=6;
                    return $error2;
                }
                else{
                    if(!($id[10]=='M'||$id[10]=='m'||$id[10]=='H'||$id[10]=='h')){//NO tiene H,h,M o m en el campo 11
                        $error2=7;
                        return $error2;
                    }
                    else{
                        $idedo[0]=$id[11];
                        $idedo[1]=$id[12];
                        $estado=implode($idedo);
                        echo $estado;
                        $bandera3=validarEdo($estado);//Si es un Edo Válido
                        if($bandera3==0){
                            $error2=8;
                            return $error2;
                        }
                        else{
                            for($i=13;$i<16;$i++) $idparte2[$i-13]=$id[$i];
                            $cadena2=implode($idparte2);
                            if(ctype_alpha($cadena2)==FALSE){//LOS 3 VALORES SON NUMEROS?
                                $error2=9;
                                return $error2;
                            }
                            else{
                                for($i=16;$i<18;$i++) $idfin[$i-16]=$id[$i];
                                $fin=implode($idfin);
                                echo $fin;
                                if(!is_numeric($fin)){
                                    $error2=10;
                                    return $error2;
                                }
                                else{
                                    $error2=0;
                                    return $error2;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    unset($idparte1);
}

?>