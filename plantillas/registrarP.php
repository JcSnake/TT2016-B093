<!DOCTYPE HTML>
<html>
    <head>
        <?php include('head.php');?>
        <title>TT 2016-B093: Registro de Pacientes</title>
    </head>
    <body>
        <div class="contenedor-formulario">
            <div class="wrapH">
                <h3>Registros Pacientes</h3><br>
                <form class="formulario" method="post" action="#">
                    <div class="input_group">
                        <input id="Curp" type="text" class="datos" name="curp">
                        <label class="label active" for="Curp">CURP:</label>
                    </div>
                    <div class="input_group">
                        <input id="ApellidoPP" type="text" class="datos" name="apellidoPP">
                        <label class="label active" for="ApellidoPP">Apellido Paterno:</label>
                    </div>
                    <div class="input_group">
                        <input id="ApellidoMP" type="text" class="datos" name="apellidoMP">
                        <label class="label active" for="ApellidoMP">Apellido Materno:</label>
                    </div>
                    <div class="input_group">
                        <input id="NombreP" type="text" class="datos" name="nombreP">
                        <label class="label active" for="NombreP">Nombre:</label>
                    </div>
                    <div class="input_group">
                        <input id="TipoCrisis" type="text" class="datos" name="tipoCrisis">
                        <label class="label active" for="TipoCrisis">Tipo de Crisis:</label>
                    </div>
                    <div class="input_group">
                        <input id="Direcc" type="text" class="datos" name="direcc">
                        <label class="label active" for="Direcc">Direcci&oacute;n:</label>
                    </div>
                    <div class="input_group">
                        <input id="CelP1" type="tel" class="" name="celP1">
                        <label class="label active" for="CelP1">N&uacute;mero de celular:</label>
                    </div>
                    <center><input class="Registrar btn btn-primary" type="submit" name="RegistrarP" value="Registrar"></center>
                </form>
            <!--?php include ('../includes/validacionRp.php') ?!-->
            </div>
        </div>
    </body>
</html>