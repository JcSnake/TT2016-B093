<!DOCTYPE HTML>
<html>
    <head>
        <?php include('head.php');?>
        <title>TT 2016-B093: Registro de Especialistas</title>
    </head>
    <body>
        <header>
            <div class="wrapper">
                <div class="imagenlogo"><img src="../imagenes/logo_sitio.png" width="250" height="50" alt=""></div>
                <div class="logo">Programa Prioritario de Epilepsia</div>
                <nav>
                    <a href="#">Cerrar Sesi&oacute;n</a>
                </nav>
            </div>
        </header>
            <div class="contenedor-formulario">
                <div class="wrapH">
                    <h3>Registro de Especialistas</h3><br>
                    <form class="formulario" method="POST" action="#">
                        <div class="input_group">
                            <input id="cedula" class="datos" type="text" name="numced" placeholder="12345678">
                            <label class="label active" for="cedula">N&uacute;mero de c&eacute;dula:</label>
                        </div>
                        <div class="input_group">
                            <input id="ApellidoP" class="datos" type="text" name="apellidoP">
                            <label class="label active" for="ApellidoP">Apellido Paterno:</label>
                        </div>
                        <div class="input_group">
                            <input id="ApellidoM" class="datos" type="text" name="apellidoM">
                            <label class="label active" for="ApellidoM">Apellido Materno:</label> 
                        </div>
                        <div class="input_group">
                            <input class="datos" id="Nombre" type="text" name="nombre">
                            <label class="label active" for="Nombre">Nombre:</label>
                        </div>
                        <div class="input_group">
                            <input id="Correo" class="datos" type="text" name="correo">
                            <select name="opcion">
                                <option value="@hotmail.com">@hotmail.com</option>
                                <option value="@gmail.com">@gmail.com</option>
                            </select>
                            <label class="label active" for="Correo">Correo:</label>
                        </div>
                        <div class="input_group">
                            <input id="Contrasena" class="datos" type="password" name="password" placeholder="(M&iacute;nimo 7 caracteres)">
                            <label class="label active" for="Contrasena">Contrase&ntilde;a:</label>
                        </div>
                        <div class="input_group">
                            <input id="ConfirmContra" class="datos" type="password" name="passwordconfirm">
                            <label class="label active" for="ConfirmContra">Confirmar contrase&ntilde;a:</label>
                        </div>
                        <button class="Registrar btn btn-primary" id="btn-registrar" type="submit" name="Registrar" >Registrar</button>
                        <a href="menuAdm.php">Regresar</a>
                    </form>
                    <?php include ('../includes/validacionR.php') ?>
                </div>
            </div>
        <footer>
            <div class="container">
                <div class="row"></div>
            </div>
        </footer>
    </body>
</html>