<?php include('../includes/validacionR.php');?>
<div class="modal fade" id="RegistroEsp" role="form" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-tittle">Registro de Especialistas</h2>
            </div>
            <div class="modal-body">
            <div class="contenedor-formulario">
                <div class="wrapH">
                    <form id="RegistrarEspecialistas" class="formulario" method="POST" action="#">
                        <div class="input_group">
                            <input id="cedula" class="datos" type="text" name="numced" placeholder="12345678">
                            <label class="label active" for="cedula">N&uacute;mero de c&eacute;dula:</label>
                        </div>
                        <div class="input_group">
                            <input id="ApellidoP" class="datos" type="text" name="apellidoP">
                            <label class="label active" for="ApellidoP">Apellido Paterno:</label>
                        </div>
                        <div class="input_group">
                            <input id="ApellidoM" class="datos" type="text" name="apellidoM">
                            <label class="label active" for="ApellidoM">Apellido Materno:</label> 
                        </div>
                        <div class="input_group">
                            <input class="datos" id="Nombre" type="text" name="nombre">
                            <label class="label active" for="Nombre">Nombre:</label>
                        </div>
                        <div class="input_group">
                            <input id="Correo" class="datos" type="text" name="correo">
                            <select name="opcion">
                                <option value="@hotmail.com">@hotmail.com</option>
                                <option value="@gmail.com">@gmail.com</option>
                            </select>
                            <label class="label active" for="Correo">Correo:</label>
                        </div>
                        <div class="input_group">
                            <input id="Contrasena" class="datos" type="password" name="password" placeholder="(M&iacute;nimo 7 caracteres)">
                            <label class="label active" for="Contrasena">Contrase&ntilde;a:</label>
                        </div>
                        <div class="input_group">
                            <input id="ConfirmContra" class="datos" type="password" name="passwordconfirm">
                            <label class="label active" for="ConfirmContra">Confirmar contrase&ntilde;a:</label>
                        </div>
                    
                </div>
            </div>
            </div>
            <div class="modal-footer">
            <button class="Registrar btn btn-primary" type="submit" name="RegistrarEspecialista" data-toggle="modal">Registrar</button>
            </form>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>