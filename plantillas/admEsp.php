<?php include_once('../includes/consultaE.php');
$consulta=consulta();?>
<!DOCTYPE HTML>
<html>
    <head>
        <?php include('head.php');?>
        <title>TT 2016-B093: INICIO</title>
    </head>
    <body>
        <header>
            <div class="wrapper">
                <div class="imagenlogo"><img src="../imagenes/logo_sitio.png" width="250" height="50" alt=""></div>
                <div class="logo">Programa Prioritario de Epilepsia</div>
                <nav>
                    <a href="#">Cerrar Sesi&oacute;n</a>
                </nav>
            </div>
        </header>
        <div class="tabla">
        <center><h2>Especialistas</h2></center>
            <form id="EliminarEsp" action="#" method="POST">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr class="table-info">
                        <th></th>
                        <th>C&eacute;dula</th>
                        <th>Nombre Completo</th>
                        <th>Correo</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $numero=1;
                        while($persona=$consulta->fetch_assoc()){
                            echo '<tr>
                                    <td><input type="checkbox" name="ckb'.$numero.'" value="'.$persona['Cedula'].'"></td>
                                    <td>'.$persona['Cedula'].'</td>
                                    <td>'.$persona['Nombre'].' '.$persona['ApellidoP'].' '.$persona['ApellidoM'].'</td>
                                    <td>'.$persona['Correo'].'</td>
                                </tr>';
                            $numero++;
                        }
                        include('../includes/eliminarEsp.php');
                        verifica($numero);
                    ?>
                </tbody>
            </table>
        <div class="opcionesE">
            <button class="btn btn-outline-danger" type="submit" name="Eliminar" href="#EliminarEsp">Eliminar</button>
            </form>
            <button class="btn btn-outline-primary" type="submit" name="Registrar" data-toggle="modal" href="#RegistroEsp" >Registrar</button>
            <a href="menuAdm.php" role="button" class="btn btn-outline-secondary">Regresar</a>
            <?php include('modalRegistroEsp.php');?>
        </div>
        </div>
        <footer>
            <div class="texto">Av. Insurgentes Sur No. 3877, Col. La Fama, Del. Tlalpan, - Tel. 5606 3822
Distrito Federal CP. 14269
            </div>
        </footer>
    </body>
</html>