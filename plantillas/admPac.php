<?php include_once('../includes/consultaP.php');
$consulta=consulta();?>
<!DOCTYPE HTML>
<html>
    <head>
        <?php include('head.php');?>
        <title>TT 2016-B093: INICIO</title>
    </head>
    <body>
        <header>
            <div class="imagenlogo"><img src="../imagenes/logo_sitio.png" width="250" height="50" alt=""></div>
            <div class="logo">Programa Prioritario de Epilepsia</div>
            <nav>
                <a href="#">Cerrar Sesi&oacute;n</a>
            </nav>
        </header>
        <div class="tabla">
            <center><h2>Pacientes</h2></center>
            <form action="#" method="POST">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr class="table-info">
                        <th></th>
                        <th>CURP</th>
                        <th>Nombre Completo</th>
                        <th>Fecha de Nacimiento</th>
                        <th>Tipo de Crisis</th>
                        <th>Direcci&oacute;n</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $numero=1;
                        while($persona=$consulta->fetch_assoc()){
                            echo '<tr>
                                <td><input type="checkbox" name="ckb'.$numero.'" value="'.$persona['Curp'].'"></td>
                                <td>'.$persona['Curp'].'</td>
                                <td>'.$persona['Nombre'].' '.$persona['ApellidoP'].' '.$persona['ApellidoM'].'</td>
                                <td>'.$persona['NacD'].'-'.$persona['NacM'].'-'.$persona['NacA'].'</td>
                                <td>'.$persona['TipoCrisis'].'</td>
                                <td>'.$persona['Direccion'].'</td>
                                </tr>';
                            $numero++;
                        }
                        include('../includes/eliminarPac.php');
                        verifica($numero);
                    ?>
                </tbody>
            </table>
            <button class="btn btn-outline-danger" type="submit" name="Eliminar">Eliminar</button>
            <input class="btn btn-outline-warning" type="submit" name="Modificar" value="Modificar">
            </form>
            <div class="opcionesE">
                    <button class="btn btn-outline-primary" type="submit" name="Registrar" data-toggle="modal" href="#RegistroPac">Registrar</button>
                    <?php include ('modalRegistroPac.php');?>
                    <input class="btn btn-outline-success" type="submit" name="VerReg" value="Ver Registros">
                    <a href="menuEsp.php" class="btn btn-outline-secondary">Regresar</a>
            </div>
        </div>
        <footer>
            <div class="texto">Av. Insurgentes Sur No. 3877, Col. La Fama, Del. Tlalpan, - Tel. 5606 3822
Distrito Federal CP. 14269
            </div>
        </footer>
    </body>
</html>