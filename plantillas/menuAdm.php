<!DOCTYPE HTML>
<html>
    <head>
        <?php include('head.php');?>
        <title>TT 2016-B093: Ventana del Administrador</title>
    </head>
    <body>
        <header>
            <div class="wrapper">
                <div class="imagenlogo"><img src="../imagenes/logo_sitio.png" width="250" height="50" alt=""></div>
                <div class="logo">Programa Prioritario de Epilepsia</div>
                <nav>
                    <a href="#">Cerrar Sesi&oacute;n</a>
                </nav>
            </div>
        </header>
        <div class="menuadm">
            <center><h2>BIENVENIDO ADMINISTRADOR</h2></center>
            <div id="VerPac">
                <a href="admPacA.php" title="">
                    <img src="../imagenes/paciente.png" width="300" height="300" alt="" /><br>
                    <center><h2>Ver Pacientes</h2></center>
                </a>
            </div>
            <div id="VerEsp">
                <a href="admEspA.php" title="">
                    <img src="../imagenes/especialista.png" width="300" height="300" alt="" /><br>
                    <center><h2>Ver Especialistas</h2></center>
                </a>
            </div>
        </div>
        <footer>
            <div class="texto">Av. Insurgentes Sur No. 3877, Col. La Fama, Del. Tlalpan, - Tel. 5606 3822
Distrito Federal CP. 14269
            </div>
        </footer>
    </body>
</html>