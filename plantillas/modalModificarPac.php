<?php include('../includes/validacionMp.php');?>
<?php include_once('../includes/consultaP.php');
$consulta=consultaUnica();?>
<div class="modal fade" id="ModificarPac" role="form" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-tittle">Modificar datos del Paciente</h2>
            </div>
            <div class="modal-body">
            <div class="contenedor-formulario">
                <div class="wrapH">
                    <form class="formulario" method="POST" action="#">
                        <div class="input_group">
                                <input id="Curp" type="text" class="datos" name="curp">
                                <label class="label active" for="Curp">CURP:</label>
                            </div>
                            <div class="input_group">
                                <input id="ApellidoPP" type="text" class="datos" name="apellidoPP">
                                <label class="label active" for="ApellidoPP">Apellido Paterno:</label>
                            </div>
                            <div class="input_group">
                                <input id="ApellidoMP" type="text" class="datos" name="apellidoMP">
                                <label class="label active" for="ApellidoMP">Apellido Materno:</label>
                            </div>
                            <div class="input_group">
                                <input id="NombreP" type="text" class="datos" name="nombreP">
                                <label class="label active" for="NombreP">Nombre:</label>
                            </div>
                            <div class="input_group">
                                <input id="TipoCrisis" type="text" class="datos" name="tipoCrisis">
                                <label class="label active" for="TipoCrisis">Tipo de Crisis:</label>
                            </div>
                            <div class="input_group">
                                <input id="Direcc" type="text" class="datos" name="direcc">
                                <label class="label active" for="Direcc">Direcci&oacute;n:</label>
                            </div>
                            <div class="input_group">
                                <input id="CelP1" type="tel" class="" name="celP1">
                                <label class="label active" for="CelP1">N&uacute;mero de celular:</label>
                            </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="Modificar btn btn-primary" type="submit" name="ModificarPaciente" data-toggle="modal" href="#ModificarPac">Modificar</button>
                </form>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>